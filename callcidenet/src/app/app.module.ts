import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { HomeComponent } from './home/home.component';
import { FooterComponent } from './footer/footer.component';
import { NavigationComponent } from './navigation/navigation.component';
import { ListStatusComponent } from './status/list-status/list-status.component';
import { JumbotronComponent } from './jumbotron/jumbotron.component';
import { TabStatusComponent } from './status/tab-status/tab-status.component';
import { CreateStatusComponent } from './status/create-status/create-status.component';
import { UpdateStatusComponent } from './status/update-status/update-status.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    HomeComponent,
    FooterComponent,
    NavigationComponent,
    ListStatusComponent,
    JumbotronComponent,
    TabStatusComponent,
    CreateStatusComponent,
    UpdateStatusComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
