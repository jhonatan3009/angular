import { Component, OnInit } from '@angular/core';
import swal from 'sweetalert2';
@Component({
  selector: 'app-update-status',
  templateUrl: './update-status.component.html',
  styleUrls: ['./update-status.component.scss']
})
export class UpdateStatusComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

  update(): boolean{
    swal.fire('Muy bien', 'Haz actualizado un estado', 'success');
    return false;
  }
}
