import { Component, OnInit } from '@angular/core';
import { MessageHeader } from 'src/app/models/messageHeader';


@Component({
  selector: 'app-tab-status',
  templateUrl: './tab-status.component.html',
  styleUrls: ['./tab-status.component.scss']
})
export class TabStatusComponent implements OnInit {
  messageHeader: MessageHeader = new MessageHeader();
  constructor() { }

  ngOnInit(): void {
    this.showMessage();
  }

  showMessage(): boolean{

    this.messageHeader.title = 'Gestionar Estados';
    this.messageHeader.description = 'Gestiona aqui los estado que puede tener los diferentes modulos de la pagina'
                         .concat('a travez de las opciones Crear Estado - Ver todos los estados.');
    return false;
  }
}
