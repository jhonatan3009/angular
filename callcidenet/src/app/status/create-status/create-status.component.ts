import { Component, OnInit } from '@angular/core';
import { Status } from '../models/status';
import swal from 'sweetalert2';


@Component({
  selector: 'app-create-status',
  templateUrl: './create-status.component.html',
  styleUrls: ['./create-status.component.scss']
})
export class CreateStatusComponent implements OnInit {
  statusA = Status;
  constructor() {

   }

  ngOnInit(): void {
  }

  save(status: string, desc: string): boolean{
    swal.fire('Muy bien', 'Haz creaado un estado nuevo', 'success');

    return false;
  }

}
