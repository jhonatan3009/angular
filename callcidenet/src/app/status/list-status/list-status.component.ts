import { Component, Input, OnInit } from '@angular/core';
import { Status } from '../models/status';

@Component({
  selector: 'app-list-status',
  templateUrl: './list-status.component.html',
  styleUrls: ['./list-status.component.scss']
})
export class ListStatusComponent implements OnInit {
  status: Status[];

  constructor() {
    this.status = [];

  }

  ngOnInit(): void {
   this.status.push({idStatus: 1, name: 'Activo', description: 'Esta activo en los registros'});
   this.status.push({idStatus: 1, name: 'Inactivo', description: 'Esta Inactivo en los registros'});
   this.status.push({idStatus: 1, name: 'Suspendido', description: 'Esta Suspendido en los registros'});
   this.status.push({idStatus: 1, name: 'Suspendido', description: 'Esta Suspendido en los registros'});
  }

}
