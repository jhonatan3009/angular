import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { LoginComponent } from './login/login.component';
import { TabStatusComponent } from './status/tab-status/tab-status.component';



const routes: Routes = [
  {path: '', component: LoginComponent},
  { path: 'home', component: HomeComponent },
  { path: 'tab-status', component: TabStatusComponent },


];
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
