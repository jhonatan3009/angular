import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'Login';
  subTitle = 'Ingrese a la plataforma de empleos de Cidenet S.A.S';
}
