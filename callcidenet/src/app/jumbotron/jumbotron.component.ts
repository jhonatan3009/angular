import { Component, Input, OnInit } from '@angular/core';
import { MessageHeader } from '../models/messageHeader';
@Component({
  selector: 'app-jumbotron',
  templateUrl: './jumbotron.component.html',
  styleUrls: ['./jumbotron.component.scss']
})
export class JumbotronComponent implements OnInit {
  @Input() message: MessageHeader;
  constructor() { }

  ngOnInit(): void {
  }

}
