import { Component, OnInit } from '@angular/core';
import { MessageHeader } from '../models/messageHeader';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  messageHeader: MessageHeader = new MessageHeader();
  constructor() {

  }

  ngOnInit(): void {
    this.showMessage();
  }

  showMessage(): boolean{

    this.messageHeader.title = 'Bienvenidos a Calls Cidenet';
    this.messageHeader.description = 'Somos una familia de desarrolladores ¿Quieres ser parte de nuestra familia?'
            .concat(' Da una vuelta en la pagina y postulate.');

    return false;
  }

}
